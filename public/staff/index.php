<?php 
require_once('../../private/initialize.php');
$page_title = "Staff Menu"; 
include(SHARED_PATH . '/staff_header.php');
require_login();
?>

<div id='content'>
<div id="main-menu">
    <h2>Main Menu</h2>
    <ul>
    <li><a href="<?php echo url_for('/staff/subjects/index.php'); ?>">Subjects</a>
    <li><a href="<?php echo url_for('/staff/pages/index.php'); ?>">Pages</a>
    <li><a href="<?php echo url_for('/staff/admins/index.php'); ?>">Admins</a>

    </ul>
    </div>
</div>

<?php include(SHARED_PATH . './staff_footer.php'); ?>


